<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <body>
        <h1>{{$title}}</h1>
        <p>{{$text}}</p>
    </body>
</html>
