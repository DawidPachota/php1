<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{
    public function showComments(){
        $comments = DB::table('comments')->get();
        return view('comments',['title'=>$comments[0]->title, 'id'=>0]);
    }
    public function showComment($id){
        $comments = DB::table('comments')->get();
        return view('comment',['title'=>$comments[$id]->title, 'text'=>$comments[$id]->text]);
    }

}
