<?php

require "menu.php";
$endpoint = explode("/", $uri);
array_shift($endpoint);
$view = "";
echo "<html lang='en'>
<head>
    <title>Pretty URL</title>
</head>
<body> 
";
switch($endpoint[0]) {
    case "about":
        require "about.php";
        echo $message;
        break;
    case "users":
        require "users.php";
        break;
    case "user":
        require "user.php";
        break;
    default:
        require "home.php";
        echo $message;
        break;
}
echo"
</body>
</html>";