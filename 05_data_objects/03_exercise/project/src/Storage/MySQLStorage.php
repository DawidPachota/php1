<?php

namespace Storage;

use Concept\Distinguishable;
use PDO;

class MySQLStorage extends SQLStorage implements Storage
{
    public function __construct()
    {
        self::$pdo = new PDO("mysql:host=127.0.0.1; port=3306; dbname=test", "test", "test123");
        self::initPDO(self::$pdo);
    }
}