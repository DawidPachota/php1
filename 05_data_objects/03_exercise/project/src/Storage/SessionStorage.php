<?php

namespace Storage;

use Concept\Distinguishable;

class SessionStorage implements Storage
{
    public function __construct()
    {
        session_start();
        $_SESSION["data"] = array();
    }

    public function store(Distinguishable $distinguishable) : void
    {
        array_push($_SESSION["data"], serialize($distinguishable));
    }

    public function loadAll(): array
    {
        $res = array();
        foreach ($_SESSION["data"] as $row)
        {
            $res[] = unserialize($row);
            $_SESSION["data"] = [];
        }
        return $res;
    }
}