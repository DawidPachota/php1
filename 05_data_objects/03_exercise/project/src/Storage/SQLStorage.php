<?php

namespace Storage;

use Concept\Distinguishable;
use PDO;

abstract class SQLStorage
{
    protected static $pdo;
    protected function initPDO($pdo) : void
    {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->exec("CREATE TABLE IF NOT EXISTS objects (`key` VARCHAR(30) PRIMARY KEY, `data` VARCHAR(200) )");
        $pdo->exec("DELETE FROM objects");
    }
    public function store(Distinguishable $distinguishable) : void
    {
        $statement = self::$pdo->prepare("INSERT INTO objects VALUES(:key, :data)");
        $statement->bindValue('key', $distinguishable->key());
        $statement->bindValue('data', serialize($distinguishable));
        $statement->execute();
    }

    public function loadAll(): array
    {
        $query = self::$pdo->query("SELECT * FROM objects");
        $res = array();
        foreach($query->fetchAll(PDO::FETCH_ASSOC) as $row){ $res[] = unserialize($row['data']); }

        return $res;
    }
}