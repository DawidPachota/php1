<?php

namespace Storage;

use Concept\Distinguishable;
use PDO;
use Config\Directory;

class SQLiteStorage extends SQLStorage implements Storage
{
    public function __construct()
    {
        self::$pdo = new PDO("sqlite:../storage/db.sqlite" );
        self::initPDO(self::$pdo);
    }
}