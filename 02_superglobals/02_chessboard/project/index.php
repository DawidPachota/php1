<?php
error_reporting(-1);
ini_set("display_errors", "On");
?>
<html lang="en">
<head>
    <title>Superglobals</title>

    <style type="text/css">
        .block {
            display: inline-block;
            width: 30px;
            height: 30px;
            padding: 0;
            margin: 0;
        }

        .block:hover {
            background-color: lightgray;
        }

        .red {
            background-color: red;
        }

        .blue {
            background-color: blue;
        }

        .green {
            background-color: green;
        }

        .gray {
            background-color: gray;
        }

        .white {
            background-color: white;
        }
    </style>
</head>
<body>

<?php
session_start();
$clicks=0;

if( !isset($_SESSION['width']) || isset($_POST['sx']) )
    $_SESSION['width'] = isset($_POST['sx']) ? $_POST['sx'] : 10;
if( !isset($_SESSION['height']) || isset($_POST['sz']) )
    $_SESSION['height'] = isset($_POST['sz']) ? $_POST['sz'] : 10;

if( isset($_POST['color']) ) {
    $color = $_POST['color'];
    setcookie('color', $color);
} else if( isset($_COOKIE['color']) ){
    $color = $_COOKIE['color'];
} else
    $color = "gray";
$_SESSION['white']=array();

/*if( isset($_GET['x']) && isset($_GET['y']) ) {
    $clicks++;
    $lastX=$_GET['x'];
    $lastY=$_GET['y'];
}
else{
    if( $_GET['x'] == $lastX || $_GET['x'] == $lastY ){
    }
}*/

for( $y=0; $y<$_SESSION['height']; $y++ )
{
    for( $x=0; $x<$_SESSION['width']; $x++ )
    {
#        if($_SESSION['white']['$x']['$y'] == 0)
            $block = "<a class=\"block " . $color . "\" href=?x=" . $x . "&z=" . $y . "></a>";
#        else
#            $block = "<a class=\"block white\" href=?x=" . $x . "&y=" . $y . "></a>";
        echo $block;
    }
    echo "<br>";
}


?>

<br/>

<form method="post">
    <label>
        Columns:
        <input type="text" name="sx">
    </label>
    <label>
        Rows:
        <input type="text" name="sz">
    </label>
    <input type="submit" value="Change">
</form>

<form method="post">
    <label>
        Color:
        <select name="color">
            <option value="gray">Gray</option>
            <option value="red">Red</option>
            <option value="green">Green</option>
            <option value="blue">Blue</option>
        </select>
    </label>
    <input type="submit" value="Change">
</form>

</body>
</html>
