<?php

namespace App\Http\Controllers;

use App\Models\Books;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    public function index()
    {
        $books = Books::all();
        if($books->isEmpty())
            $books = [];
        session(['link' => url()->previous()]);
        return view('books.index')->withBooks($books);
    }

    public function show($id)
    {
        $book = Books::find($id);
        return view('books.show')->withBook($book);
    }

    public function create()
    {
        return view('books.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'isbn' => 'required|string|digits:13',
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:1023',
        ]);
        $input = $request->all();
        $book = Books::create($input);
        $id = $book->id;
        return redirect()->route('books.show', ['id' => $id]);
    }

    public function edit($id)
    {
        $book = Books::find($id);
        return view('books.edit', ['id' => $id])->withBook($book);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'isbn' => 'required|string|digits:13',
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:1023',
        ]);
        $book = Books::findOrFail($id);
        $input = $request->all();

        $book->fill($input)->save();
        return redirect()->route('books.show', ['id' => $id])->withBook($book);
    }

    public function delete($id)
    {
        $book = Books::find($id);
        $book->delete();

        return index();
    }
}
