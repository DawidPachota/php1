<?php

use App\Http\Controllers\BooksController;
use Illuminate\Support\Facades\Route;

Route::get('/books/create', [BooksController::class, 'create'])
    ->name('books.create');

Route::post('/books/create', [BooksController::class, 'store']);


Route::get('/books/{id}', [BooksController::class, 'show'])
    ->name('books.show');

Route::delete('/books/{id}', [BooksController::class, 'delete'])
    ->name('books.delete');

Route::get('/books/{id}/edit', [BooksController::class, 'edit'])
    ->name('books.edit');

Route::post('/books/{id}/edit', [BooksController::class, 'update']);

