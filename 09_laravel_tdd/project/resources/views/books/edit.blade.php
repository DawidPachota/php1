<x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>
        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <div>
            <h2>Editing a book</h2>
        </div>

        <form method="POST" action="{{ route('books.edit', $book->id) }}">
            @csrf
                <div>
                    <x-label for="isbn" :value="__('Isbn')" />

                    <x-input id="isbn" class="block mt-1 w-full" name="isbn" :value="old('isbn', $book->isbn)" required autofocus />
                </div>

                <div>
                    <x-label for="title" :value="__('Title')" />

                    <x-input id="title" class="block mt-1 w-full" name="title" :value="old('title', $book->title)" required autofocus />
                </div>

                <div>
                    <x-label for="description" :value="__('Description')" />

                    <x-input id="description" class="block mt-1 w-full" name="description" :value="old('description', $book->description)"/>
                </div>


            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-3">
                    {{ __('Update') }}
                </x-button>
            </div>
        </form>

    </x-auth-card>
</x-guest-layout>
