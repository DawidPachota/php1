<x-guest-layout>
    <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
        <div>
            <h2>List of books</h2>
        </div>

        <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
            @if ( $books == [])
                No books in database.
            @else
                @foreach ($books ?? '' as $book)
                    <li>

                        <tr>
                            <td>
                                {{$book->isbn}}
                            </td><br>
                            <td>
                                {{$book->title}}
                            </td><br>
                        </tr>
                        <a href="/books/{{ $book->id }}">Details</a><br>
                    </li>
                @endforeach
            @endif
        </div>
        <div class="flex items-center justify-end mt-4">
            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('books.create') }}">
                {{ __('Create new...') }}
            </a>
        </div>
    </div>
</x-guest-layout>
