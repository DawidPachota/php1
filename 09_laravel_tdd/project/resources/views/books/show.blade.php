<x-guest-layout>
    <div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
        <div>
            <h2>Viewing a book</h2>
        </div>

        <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
                {{ $book->isbn }}
                <br>
                {{ $book->title }}
                <br>
                {{ $book->description }}
                <br>
                <strong>{{ $book->description }}</strong>
        </div>
        <div class="flex items-center justify-end mt-4">
            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('books.edit', $book->id) }}">
                {{ __('Edit') }}
            </a>
        </div>
        <form method="DELETE" action="{{ route('books.delete', $book->id) }}">
            <div class="flex items-center justify-end mt-4">
                <x-button class="ml-3">
                    {{ __('Delete') }}
                </x-button>
            </div>
        </form>
    </div>
</x-guest-layout>
