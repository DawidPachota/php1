<?php

namespace Storage;

use Concept\Distinguishable;
use Predis\Client;

class RedisStorage implements Storage
{
    public Client $redisContainer;

    public function __construct()
    {
        $this->redisContainer = new Client();
    }

    public function store(Distinguishable $distinguishable) : void
    {
        $this->redisContainer->set($distinguishable->key(),serialize($distinguishable));
    }

    public function loadAll(): array
    {
        $result = [];
        foreach ($this->redisContainer->keys("*") as $key)
            $result[] = unserialize($this->redisContainer->get($key));
        return $result;
    }
}