<?php

namespace String;

class Editor
{
    private $string;

    public function __construct(string $string)
    {
        $this->string = $string;
    }

    public function get(): string
    {
        return $this->string;
    }

    public function replace(string $pattern, string $replace)
    {
        $this->string = preg_replace("/".$pattern."/", $replace, $this->string);
        return $this;
    }

    public function lower()
    {
        $this->string =  strtolower($this->string);
        return $this;
    }

    public function upper()
    {
        $this->string =  strtoupper($this->string);
        return $this;
    }

    public function censor(string $pattern)
    {
        $this->string = preg_replace("/".$pattern."/", str_repeat("*",strlen($pattern)), $this->string);
        return $this;
    }

    public function repeat(string $pattern, int $multiply)
    {
        $this->string = preg_replace("/".$pattern."/", str_repeat($pattern, $multiply), $this->string);
        return $this;
    }

    public function remove(string $pattern)
    {
        $this->string = preg_replace("/".$pattern."/", "", $this->string);
        return $this;
    }



}
