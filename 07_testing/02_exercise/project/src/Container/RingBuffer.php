<?php

namespace Container;

class RingBuffer
{
    private $capacity;
    private int $size = 0;
    private int $headAddress = 0;
    private int $tailAddress = 0;
    private $elements = [];

    public function __construct(string $capacity)
    {
        $this->capacity = $capacity;
    }

    public function empty(): bool
    {
        return ($this->size == 0);
    }

    public function capacity(): string
    {
        return $this->capacity;
    }

    public function size(): int
    {
        return $this->size;
    }

    public function full(): bool
    {
        if($this->size == $this->capacity)
            return true;
        return false;
    }
    
    public function push( $element )
    {
        if($this->size < $this->capacity)
            $this->size++;
        else
            $this->tailAddress++;
        $this->elements[$this->headAddress % $this->capacity] = $element;
        $this->headAddress++;
    }

    public function pop()
    {
        if( $this->size == 0 )
            return null;
        $this->size--;
        $this->tailAddress++;

        return $this->elements[($this->tailAddress - 1) % $this->capacity];
    }

    public function tail()
    {
        if( $this->size == 0 )
            return null;
        return $this->elements[$this->tailAddress % $this->capacity];
    }

    public function head()
    {
        if( $this->size == 0 )
            return null;
        return $this->elements[($this->headAddress - 1) % $this->capacity];
    }

    public function at(int $address)
    {
        if($address < 0 || $address >= $this->size())
            return null;
        return $this->elements[$address];
    }

}