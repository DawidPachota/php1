<?php

namespace Controller;

use Model\User;

class AuthController extends Controller
{
    public function login()
    {
        return ["auth.login.index", ["title" => "Login"]];
    }
    public function register()
    {
        return ["auth.register.index", ["title" => "Register"]];
    }
    public function confirmation_notice()
    {
        $id = $_POST["id"];
        $name = $_POST["name"];
        $surname = $_POST["surname"];
        $email = $_POST["email"];
        $password_hashed = password_hash($_POST["password"], PASSWORD_DEFAULT);

        $storage = $this->storage("mysql");

        $user = new User($id, $name, $surname, $email, $password_hashed);
        $storage->store( $user );

        return ["auth.confirmation_notice.index", ["title" => "Confirmation notice"]];
    }
}