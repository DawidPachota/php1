<?php

namespace Model;

use Concept\Distinguishable;

class User extends Distinguishable
{
    public string $name;
    public string $surname;
    public string $email;
    public string $password;
    public string $token;
    public bool $confirmed;


    public function __construct(int $id, string $name, string $surname, string $email, string $password)
    {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->password = $password;
        $this->confirmed = false;

        $this->token = openssl_random_pseudo_bytes(16);
        $this->token = bin2hex($this->token);
    }
}