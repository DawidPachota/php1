<?php

namespace Config;

class Directory
{
    private static string $rootPath;
    private static string $storage = "storage/";
    private static string $src = "src/";
    private static string $view = "view/";

    public static function set($root)
    {
       	Directory::$rootPath = $root;
    }

    public static function root()
    {
        if (isset(self::$rootPath))
            return self::$rootPath;
        else
            return "root path not set";
    }

    public static function storage()
    {
        return self::$rootPath . self::$storage;
    }

    public static function src()
    {
        return self::$rootPath . self::$src;
    }

    public static function view()
    {
        return self::$rootPath . self::$view;
    }


}
