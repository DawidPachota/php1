<?php

use Widget\Button;
use Widget\Link;
use Storage\FileStorage;

class App
{
    public function run() {
        $storage = new FileStorage();

        $appWidgets = [
            new Link(1),
            new Button(1),
            new Button(2),
            new Link(2),
            new Link(3),
            new Button(3)
        ];

        foreach( $appWidgets as $widgetToStore )
            $storage->store($widgetToStore);

        foreach( $storage->loadAll() as $widgetToDraw )
            $this->render($widgetToDraw);
    }

    private function render($widgetToDraw)
    {
        $widgetToDraw->draw();
    }
}
