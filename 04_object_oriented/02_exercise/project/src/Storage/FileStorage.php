<?php

namespace Storage;
use Config\Directory;
use Concept\Distinguishable;

class FileStorage implements Storage{
    public function store(Distinguishable $distinguishable)
    {
        $name = $distinguishable->key();
        $content = serialize($distinguishable);
        $path = Directory::storage() . $name;

        $fileHandle = fopen($path, "w+");
        fwrite($fileHandle,$content);
        fclose($fileHandle);
    }

    public function loadAll()
    {
        $allFiles = array();
        $searchDirectory = Directory::storage();
        foreach( scandir($searchDirectory) as $file ){
            if( !preg_match("/^\./", $file) ){
                $filePath = $searchDirectory . $file;

                $fileHandle = fopen( $filePath, "r");
                $content = fread($fileHandle, filesize($searchDirectory));
                fclose($fileHandle);

                array_push($allFiles, unserialize($content));
            }
        }
        return $allFiles;
    }
}
