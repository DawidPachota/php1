<?php

namespace Concept;

abstract class Distinguishable{
    private string $id;

    public function __construct($id){
        $this->id = $this->normalize(static::class) . $id;
    }

    private function normalize(string $class)
    {
        $classTree = explode("\\", $class);
        $name = "";
        for( $i=0; $i<count($classTree); $i++ )
            $name = $name . $classTree[$i] . "_";

        return strtolower($name);
    }

    public function key(){
        return $this->id;
    }
}